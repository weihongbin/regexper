FROM node:14-alpine AS builder
WORKDIR app
COPY . .
RUN yarn config set registry https://registry.npmmirror.com \
    &&  yarn config set disturl https://npmmirror.com/dist -g \
    && yarn config set sass_binary_site https://npmmirror.com/mirrors/node-sass/ -g \
    && yarn && yarn build

FROM nginx:alpine
COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 80
